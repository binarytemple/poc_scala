import xml.{PrettyPrinter, NodeSeq}

object XMLTesting extends App {
  var rows: NodeSeq = NodeSeq.Empty

  def genContent(i: Int) = {
    <dog>1</dog>
      <cat>0</cat>
  }

  for (i <- Range(0, 10)) {
    rows = rows ++ <row>
      {genContent(i)}
    </row>
  }

  val res = <table>
    {rows}
  </table>

  val printer: PrettyPrinter = new PrettyPrinter(50, 1)
  //XML.loadString()
  val sb = new StringBuilder(1000)
  printer.format(res, sb)
  print(sb.toString())

}
