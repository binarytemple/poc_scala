import java.text.SimpleDateFormat
import java.util.Date

object ImplicitDateParsingMain extends App {

  val ISO8601_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

  implicit def stringToDate(date: String) = {
    ISO8601_DATE_FORMAT.parse(date)
  }

  implicit def stringToDates(dates: String): Seq[Date] = {
    def tryParse(s: String): Option[Date] = {
      try {
        Some(stringToDate(s.trim))
      }
      catch {
        case _ => None
      }
    }

    def splitter(s: String): Seq[Date] = s.split("\n").toSeq flatMap {
      tryParse
    }

    splitter(if (dates.contains("|")) dates.stripMargin else dates)
  }

  val datestring =
    """
      |    2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |    2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
    """

  val dates: Seq[Date] = datestring
  for(d <- dates) {
    println(d)
  }
}
