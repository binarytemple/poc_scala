import java.text.SimpleDateFormat
import java.util.Date

object DateParsingMain extends App {

  val ISO8601_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

  val dates =
    """
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
      |2012-09-23T16:26:00
    """.stripMargin.split('\n').filter(_.length > 0).filter(_.contains("20"))
  for (line <- dates) {
    println(line)
  }

  for (line <- dates) {
    val parse: Date = ISO8601_DATE_FORMAT.parse(line)
    println("date: " + parse)
  }
}
